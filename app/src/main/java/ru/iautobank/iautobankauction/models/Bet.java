package ru.iautobank.iautobankauction.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bet {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("sum")
    @Expose
    private String sum;
    @SerializedName("date")
    @Expose
    private String date;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Bet withId(int id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The sum
     */
    public String getSum() {
        return sum;
    }

    /**
     *
     * @param sum
     * The sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }

    public Bet withSum(String sum) {
        this.sum = sum;
        return this;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    public Bet withDate(String date) {
        this.date = date;
        return this;
    }

}