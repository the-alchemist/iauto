package ru.iautobank.iautobankauction.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.iautobank.iautobankauction.Constants;

public class Auction {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("lastPrice")
    @Expose
    private int lastPrice;
    @SerializedName("lastPriceFormatted")
    @Expose
    private String lastPriceFormatted;
    @SerializedName("lastDelta")
    @Expose
    private int lastDelta;
    @SerializedName("betCount")
    @Expose
    private int betCount;
    @SerializedName("fileId")
    @Expose
    private int fileId;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("dateUpdate")
    @Expose
    private String dateUpdate;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("dateEnd")
    @Expose
    private Object dateEnd;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    public String getIdString() {
        return id + "";
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Auction withId(int id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The model
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @param model
     * The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    public Auction withModel(String model) {
        this.model = model;
        return this;
    }

    /**
     *
     * @return
     * The brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @param brand
     * The brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Auction withBrand(String brand) {
        this.brand = brand;
        return this;
    }

    /**
     *
     * @return
     * The year
     */
    public String getYear() {
        return year;
    }

    /**
     *
     * @param year
     * The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    public Auction withYear(String year) {
        this.year = year;
        return this;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Auction withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     *
     * @return
     * The lastPrice
     */
    public int getLastPrice() {
        return lastPrice;
    }

    /**
     *
     * @param lastPrice
     * The lastPrice
     */
    public void setLastPrice(int lastPrice) {
        this.lastPrice = lastPrice;
    }

    public Auction withLastPrice(int lastPrice) {
        this.lastPrice = lastPrice;
        return this;
    }

    /**
     *
     * @return
     * The lastPriceFormatted
     */
    public String getLastPriceFormatted() {
        return lastPriceFormatted;
    }

    /**
     *
     * @param lastPriceFormatted
     * The lastPriceFormatted
     */
    public void setLastPriceFormatted(String lastPriceFormatted) {
        this.lastPriceFormatted = lastPriceFormatted;
    }

    public Auction withLastPriceFormatted(String lastPriceFormatted) {
        this.lastPriceFormatted = lastPriceFormatted;
        return this;
    }

    /**
     *
     * @return
     * The lastDelta
     */
    public int getLastDelta() {
        return lastDelta;
    }

    /**
     *
     * @param lastDelta
     * The lastDelta
     */
    public void setLastDelta(int lastDelta) {
        this.lastDelta = lastDelta;
    }

    public Auction withLastDelta(int lastDelta) {
        this.lastDelta = lastDelta;
        return this;
    }

    /**
     *
     * @return
     * The betCount
     */
    public int getBetCount() {
        return betCount;
    }

    /**
     *
     * @param betCount
     * The betCount
     */
    public void setBetCount(int betCount) {
        this.betCount = betCount;
    }

    public Auction withBetCount(int betCount) {
        this.betCount = betCount;
        return this;
    }

    /**
     *
     * @return
     * The fileId
     */
    public int getFileId() {
        return fileId;
    }

    /**
     *
     * @param fileId
     * The fileId
     */
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public Auction withFileId(int fileId) {
        this.fileId = fileId;
        return this;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return Constants.API_IMAGE_URL + getFileId();
    }

    /**
     *
     * @param imageUrl
     * The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Auction withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     *
     * @return
     * The dateUpdate
     */
    public String getDateUpdate() {
        return dateUpdate;
    }

    /**
     *
     * @param dateUpdate
     * The dateUpdate
     */
    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Auction withDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
        return this;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Auction withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     *
     * @return
     * The dateEnd
     */
    public Object getDateEnd() {
        return dateEnd;
    }

    /**
     *
     * @param dateEnd
     * The dateEnd
     */
    public void setDateEnd(Object dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Auction withDateEnd(Object dateEnd) {
        this.dateEnd = dateEnd;
        return this;
    }

}