package ru.iautobank.iautobankauction.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.ArrayList;

public class Lot {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("lastBet")
    @Expose
    private String lastBet;
    @SerializedName("image")
    @Expose
    private int image;
    @SerializedName("images")
    @Expose
    private List<Image> images = new ArrayList<Image>();
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("run")
    @Expose
    private String run;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("shop")
    @Expose
    private String shop;
    @SerializedName("bets")
    @Expose
    private List<Bet> bets = new ArrayList<Bet>();

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Lot withId(int id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(String result) {
        this.result = result;
    }

    public Lot withResult(String result) {
        this.result = result;
        return this;
    }

    /**
     *
     * @return
     * The brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @param brand
     * The brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTitle() {
        return this.brand + " " + this.model + ", " + this.year;
    }

    public Lot withBrand(String brand) {
        this.brand = brand;
        return this;
    }

    /**
     *
     * @return
     * The model
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @param model
     * The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    public Lot withModel(String model) {
        this.model = model;
        return this;
    }

    /**
     *
     * @return
     * The year
     */
    public String getYear() {
        return year;
    }

    /**
     *
     * @param year
     * The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    public Lot withYear(String year) {
        this.year = year;
        return this;
    }

    /**
     *
     * @return
     * The lastBet
     */
    public String getLastBet() {
        return lastBet;
    }

    /**
     *
     * @param lastBet
     * The lastBet
     */
    public void setLastBet(String lastBet) {
        this.lastBet = lastBet;
    }

    public Lot withLastBet(String lastBet) {
        this.lastBet = lastBet;
        return this;
    }

    /**
     *
     * @return
     * The image
     */
    public int getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(int image) {
        this.image = image;
    }

    public Lot withImage(int image) {
        this.image = image;
        return this;
    }

    /**
     *
     * @return
     * The images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     *
     * @param images
     * The images
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Lot withImages(List<Image> images) {
        this.images = images;
        return this;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    public Lot withDate(String date) {
        this.date = date;
        return this;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    public Lot withComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     *
     * @return
     * The run
     */
    public String getRun() {
        return run;
    }

    /**
     *
     * @param run
     * The run
     */
    public void setRun(String run) {
        this.run = run;
    }

    public Lot withRun(String run) {
        this.run = run;
        return this;
    }

    /**
     *
     * @return
     * The body
     */
    public String getBody() {
        return body;
    }

    /**
     *
     * @param body
     * The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    public Lot withBody(String body) {
        this.body = body;
        return this;
    }

    /**
     *
     * @return
     * The capacity
     */
    public String getCapacity() {
        return capacity;
    }

    /**
     *
     * @param capacity
     * The capacity
     */
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public Lot withCapacity(String capacity) {
        this.capacity = capacity;
        return this;
    }

    /**
     *
     * @return
     * The power
     */
    public String getPower() {
        return power;
    }

    /**
     *
     * @param power
     * The power
     */
    public void setPower(String power) {
        this.power = power;
    }

    public Lot withPower(String power) {
        this.power = power;
        return this;
    }

    /**
     *
     * @return
     * The transmission
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     *
     * @param transmission
     * The transmission
     */
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public Lot withTransmission(String transmission) {
        this.transmission = transmission;
        return this;
    }

    /**
     *
     * @return
     * The shop
     */
    public String getShop() {
        return shop;
    }

    /**
     *
     * @param shop
     * The shop
     */
    public void setShop(String shop) {
        this.shop = shop;
    }

    public Lot withShop(String shop) {
        this.shop = shop;
        return this;
    }

    /**
     *
     * @return
     * The bets
     */
    public List<Bet> getBets() {
        return bets;
    }

    /**
     *
     * @param bets
     * The bets
     */
    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public Lot withBets(List<Bet> bets) {
        this.bets = bets;
        return this;
    }

}