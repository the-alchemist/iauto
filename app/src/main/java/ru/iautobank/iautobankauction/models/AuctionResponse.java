package ru.iautobank.iautobankauction.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by thealchemist on 6/6/16.
 */
public class AuctionResponse {
    private Auction[] auctions;

    public Auction[] getAuctions() {
        return auctions;
    }

    public List<Auction> getAuctionList() {
        List<Auction> auction = new ArrayList<>();
        Collections.addAll(auction, auctions);
        return auction;
    }

}
