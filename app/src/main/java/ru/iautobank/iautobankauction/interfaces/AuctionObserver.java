package ru.iautobank.iautobankauction.interfaces;

import ru.iautobank.iautobankauction.models.Auction;

/**
 * Created by thealchemist on 6/6/16.
 */
public interface AuctionObserver<T> {
    void getAuctionsChanged(T auctions);
}
