package ru.iautobank.iautobankauction.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.iautobank.iautobankauction.Constants;
import ru.iautobank.iautobankauction.models.Auction;
import ru.iautobank.iautobankauction.models.AuctionResponse;
import ru.iautobank.iautobankauction.models.Lot;

/**
 * Created by thealchemist on 5/17/16.
 */
public interface AutoAPI {
    @GET("auction-list")
    Call<List<Auction>> getAuctions();
    @GET("auction-view")
    Call<Lot> getLot(@Query("id") int id);

    class Factory {
        private static AutoAPI service;
        public static AutoAPI getInstance() {
            if(service == null) {
                Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.API_URL).addConverterFactory(GsonConverterFactory.create()).build();
                service = retrofit.create(AutoAPI.class);
                return service;
            } else {
                return service;
            }
        }
    }
}
