package ru.iautobank.iautobankauction.managers;

import java.util.List;

import ru.iautobank.iautobankauction.models.Auction;

/**
 * Created by thealchemist on 6/20/16.
 */
public interface AuctionManager {
    public void addAuction(Auction auction);
    public void addAuctions(List<Auction> auctions);
    public List<Auction> getAllAuctions();
    public int getAuctionCount();
    public boolean isDatabaseEmpty();
}
