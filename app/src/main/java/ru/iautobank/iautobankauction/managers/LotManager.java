package ru.iautobank.iautobankauction.managers;

import java.util.List;

import ru.iautobank.iautobankauction.models.Lot;

/**
 * Created by thealchemist on 6/21/16.
 */
public interface LotManager {
    public void addLot(Lot lot);
    public Lot getLot();
    public int getCount();
    public boolean isDatabaseEmpty();
}
