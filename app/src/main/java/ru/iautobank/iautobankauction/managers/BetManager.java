package ru.iautobank.iautobankauction.managers;

import java.util.List;

import ru.iautobank.iautobankauction.models.Bet;

/**
 * Created by thealchemist on 6/20/16.
 */
public interface BetManager {
    public void addBet(Bet bet);
    public void addBets(List<Bet> bets);
    public List<Bet> getAllBets();
    public int getBetCount();
    public boolean isDatabaseEmpty();

}
