package ru.iautobank.iautobankauction.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import ru.iautobank.iautobankauction.Constants;
import ru.iautobank.iautobankauction.managers.AuctionManager;
import ru.iautobank.iautobankauction.models.Auction;

/**
 * Created by thealchemist on 5/29/16.
 */
public class AuctionHelper extends SQLiteOpenHelper implements BaseColumns, AuctionManager {

    private List<Auction> auctions;
    private static AuctionHelper instance;

    public static synchronized AuctionHelper getInstance(Context context) {
        if(instance == null)
            instance = new AuctionHelper(context);

        return instance;
    }

    private AuctionHelper(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        auctions = new ArrayList<>();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.AUCTION_TABLE_CREATE_SCRIPT);

        ContentValues values = new ContentValues();

        for(Auction auction: auctions) {
            populate(values, auction);
        }

        db.insert(Constants.AUCTION_TABLE, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.QUERY_DROP_AUCTIONS);
        onCreate(db);
    }

    @Override
    public void addAuction(Auction auction) {
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            populate(values, auction);
            db.insert(Constants.AUCTION_TABLE, null, values);
        } finally {
            db.close();
        }
    }

    private void populate(ContentValues values, Auction auction) {
        values.put(Constants.KEY_ID, auction.getId());
        values.put(Constants.KEY_MODEL, auction.getModel());
        values.put(Constants.KEY_BRAND, auction.getBrand());
        values.put(Constants.KEY_YEAR, auction.getYear());
        values.put(Constants.KEY_TITLE, auction.getTitle());
        values.put(Constants.KEY_LASTPRICE, auction.getLastPrice());
        values.put(Constants.KEY_LASTPRICEFORMATTED, auction.getLastPriceFormatted());
        values.put(Constants.KEY_LASTDELTA, auction.getLastDelta());
        values.put(Constants.KEY_BETCOUNT, auction.getBetCount());
        values.put(Constants.KEY_FILEID, auction.getFileId());
        values.put(Constants.KEY_IMAGEURL, auction.getImageUrl());
        values.put(Constants.KEY_DATEUPDATE, auction.getDateUpdate());
        values.put(Constants.KEY_CREATEDAT, auction.getCreatedAt());
        values.put(Constants.KEY_DATEEND, (String) auction.getDateEnd());
    }

    @Override
    public void addAuctions(List<Auction> auctions) {
        for(Auction auction: auctions) {
            addAuction(auction);
        }
    }

    @Override
    public List<Auction> getAllAuctions() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Auction> auctions = new ArrayList<>();
        Cursor cursor = db.rawQuery(Constants.QUERY_GET_AUCTIONS, null);
        if(!cursor.isLast()) {
            while (cursor.moveToNext()) {
                Auction auction = new Auction()
                        .withId(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ID)))
                        .withModel(cursor.getString(cursor.getColumnIndex(Constants.KEY_MODEL)))
                        .withBrand(cursor.getString(cursor.getColumnIndex(Constants.KEY_BRAND)))
                        .withYear(cursor.getString(cursor.getColumnIndex(Constants.KEY_YEAR)))
                        .withTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_TITLE)))
                        .withLastPrice(cursor.getInt(cursor.getColumnIndex(Constants.KEY_LASTPRICE)))
                        .withLastPriceFormatted(cursor.getString(cursor.getColumnIndex(Constants.KEY_LASTPRICEFORMATTED)))
                        .withLastDelta(cursor.getInt(cursor.getColumnIndex(Constants.KEY_LASTDELTA)))
                        .withBetCount(cursor.getInt(cursor.getColumnIndex(Constants.KEY_BETCOUNT)))
                        .withFileId(cursor.getInt(cursor.getColumnIndex(Constants.KEY_FILEID)))
                        .withImageUrl(cursor.getString(cursor.getColumnIndex(Constants.KEY_IMAGEURL)))
                        .withDateUpdate(cursor.getString(cursor.getColumnIndex(Constants.KEY_DATEUPDATE)))
                        .withCreatedAt(cursor.getString(cursor.getColumnIndex(Constants.KEY_CREATEDAT)))
                        .withDateEnd(cursor.getString(cursor.getColumnIndex(Constants.KEY_DATEEND)));
                auctions.add(auction);
            }
        }

        cursor.close();
        db.close();

        return auctions;

    }

    @Override
    public int getAuctionCount() {
        int count;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Constants.QUERY_GET_AUCTIONS, null);

        count = cursor.getCount();

        cursor.close();
        db.close();

        return count;
    }

    @Override
    public boolean isDatabaseEmpty() {
        return getAuctionCount() == 0;
    }
}
