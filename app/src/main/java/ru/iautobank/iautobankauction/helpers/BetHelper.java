package ru.iautobank.iautobankauction.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import ru.iautobank.iautobankauction.Constants;
import ru.iautobank.iautobankauction.managers.BetManager;
import ru.iautobank.iautobankauction.models.Bet;

/**
 * Created by thealchemist on 5/29/16.
 */
public class BetHelper extends SQLiteOpenHelper implements BaseColumns, BetManager {

    private List<Bet> bets;

    public BetHelper(Context context, List<Bet> bets) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        this.bets = bets;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.BET_TABLE_CREATE_SCRIPT);

        ContentValues values = new ContentValues();

        for(Bet bet: bets) {
            populate(bet, values);
        }

        db.insert(Constants.BET_TABLE, null, values);
        db.close();

    }

    private void populate(Bet bet, ContentValues values) {
        values.put(BaseColumns._ID, bet.getId());
        values.put(Constants.KEY_DATE, bet.getDate());
        values.put(Constants.KEY_SUM, bet.getSum());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.QUERY_DROP_BETS);
        onCreate(db);
    }

    @Override
    public void addBet(Bet bet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        populate(bet, values);

        db.insert(Constants.BET_TABLE, null, values);
        db.close();


    }

    @Override
    public void addBets(List<Bet> bets) {
        for(Bet bet: bets) {
            addBet(bet);
        }
    }

    @Override
    public List<Bet> getAllBets() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Bet> bets = new ArrayList<>();

        Cursor cursor = db.rawQuery(Constants.QUERY_GET_BETS, null);
        if(!cursor.isLast()) {
            while (cursor.moveToNext()) {
                Bet bet = new Bet()
                        .withId(cursor.getInt(cursor.getColumnIndex(BaseColumns._ID)))
                        .withDate(cursor.getString(cursor.getColumnIndex(Constants.KEY_DATE)))
                        .withSum(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUM)));
                bets.add(bet);
            }
        }

        cursor.close();
        db.close();

        return bets;
    }

    @Override
    public int getBetCount() {
        int count;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Constants.QUERY_GET_BETS, null);

        count = cursor.getCount();

        cursor.close();
        db.close();

        return count;
    }

    @Override
    public boolean isDatabaseEmpty() {
        return getBetCount() == 0;
    }

}
