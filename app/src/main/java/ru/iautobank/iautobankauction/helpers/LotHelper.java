package ru.iautobank.iautobankauction.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import ru.iautobank.iautobankauction.Constants;
import ru.iautobank.iautobankauction.managers.LotManager;
import ru.iautobank.iautobankauction.models.Bet;
import ru.iautobank.iautobankauction.models.Image;
import ru.iautobank.iautobankauction.models.Lot;

/**
 * Created by thealchemist on 5/29/16.
 */
public class LotHelper extends SQLiteOpenHelper implements BaseColumns, LotManager {

    Lot lot;

    private static LotHelper instance;

    public static LotHelper getInstance(Context c) {
        if(instance == null) {
            instance = new LotHelper(c);
        }
        return instance;
    }

    private LotHelper(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.LOT_TABLE_CREATE_SCRIPT);

        ContentValues values = new ContentValues();
        populate(values, lot);

        db.insert(Constants.LOT_TABLE, null, values);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constants.QUERY_DROP_LOTS);
        onCreate(db);
    }

    @Override
    public void addLot(Lot lot) {
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            populate(values, lot);
            db.insert(Constants.AUCTION_TABLE, null, values);
        } finally {
            db.close();
        }
    }

    private void populate(ContentValues values, Lot lot) {
        values = new ContentValues();

        values.put(Constants.KEY_ID, lot.getId());
        values.put(Constants.KEY_RESULT, lot.getResult());
        values.put(Constants.KEY_MODEL, lot.getModel());
        values.put(Constants.KEY_BRAND, lot.getBrand());
        values.put(Constants.KEY_YEAR, lot.getYear());
        values.put(Constants.KEY_LASTBET, lot.getLastBet());
        values.put(Constants.KEY_IMAGE, lot.getImage());
        for(Image image: lot.getImages()) {
            values.put(Constants.KEY_IMAGES, image.getLink());
        }
        values.put(Constants.KEY_DATE, lot.getDate());
        values.put(Constants.KEY_COMMENT, lot.getComment());
        values.put(Constants.KEY_RUN, lot.getRun());
        values.put(Constants.KEY_BODY, lot.getBody());
        values.put(Constants.KEY_CAPACITY, lot.getCapacity());
        values.put(Constants.KEY_POWER, lot.getPower());
        values.put(Constants.KEY_TRANSMISSION, lot.getTransmission());
        values.put(Constants.KEY_SHOP, lot.getShop());
        for(Bet bet: lot.getBets()) {
            values.put(Constants.KEY_BETS, bet.getId());
        }

    }

    @Override
    public Lot getLot() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(Constants.QUERY_GET_LOTS, null);
        if(!cursor.isLast()) {
            while (cursor.moveToNext()) {
                Lot lot = new Lot()
                        .withId(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ID)))
                        .withResult(cursor.getString(cursor.getColumnIndex(Constants.KEY_RESULT)))
                        .withModel(cursor.getString(cursor.getColumnIndex(Constants.KEY_MODEL)))
                        .withBrand(cursor.getString(cursor.getColumnIndex(Constants.KEY_BRAND)))
                        .withYear(cursor.getString(cursor.getColumnIndex(Constants.KEY_YEAR)))
                        .withLastBet(cursor.getString(cursor.getColumnIndex(Constants.KEY_LASTBET)))
                        .withImage(cursor.getInt(cursor.getColumnIndex(Constants.KEY_IMAGE)))
                        .withDate(cursor.getString(cursor.getColumnIndex(Constants.KEY_DATE)))
                        .withComment(cursor.getString(cursor.getColumnIndex(Constants.KEY_COMMENT)))
                        .withRun(cursor.getString(cursor.getColumnIndex(Constants.KEY_RUN)))
                        .withBody(cursor.getString(cursor.getColumnIndex(Constants.KEY_BODY)))
                        .withCapacity(cursor.getString(cursor.getColumnIndex(Constants.KEY_CAPACITY)))
                        .withPower(cursor.getString(cursor.getColumnIndex(Constants.KEY_POWER)))
                        .withTransmission(cursor.getString(cursor.getColumnIndex(Constants.KEY_TRANSMISSION)))
                        .withShop(cursor.getString(cursor.getColumnIndex(Constants.KEY_SHOP)));

                //TODO: bet logic for db


            }
        }

        cursor.close();
        db.close();

        return lot;
    }

    @Override
    public int getCount() {
        int count;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Constants.QUERY_GET_LOTS, null);

        count = cursor.getCount();

        cursor.close();
        db.close();

        return count;
    }

    @Override
    public boolean isDatabaseEmpty() {
        // TODO: 6/21/16
        return false;
    }
}
