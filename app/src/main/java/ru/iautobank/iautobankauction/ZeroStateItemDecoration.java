package ru.iautobank.iautobankauction;

import android.graphics.Canvas;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import static android.view.View.MeasureSpec.makeMeasureSpec;

/**
 * Created by thealchemist on 6/7/16.
 */
public class ZeroStateItemDecoration extends RecyclerView.ItemDecoration {
    private final int layoutId;
    private View view;
    private boolean enabled = true;


    public ZeroStateItemDecoration(@LayoutRes int layoutId) {
        this.layoutId = layoutId;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if(enabled && parent.getChildCount() == 0) {
            if(view == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                view = inflater.inflate(layoutId, parent, false);
            }

            view.measure(makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY), makeMeasureSpec(parent.getHeight(), View.MeasureSpec.EXACTLY));
            view.layout(parent.getLeft(), parent.getTop(), parent.getRight(), parent.getBottom());
            view.draw(c);
        }
    }
}
