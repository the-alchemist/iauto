package ru.iautobank.iautobankauction;

/**
 * Created by thealchemist on 5/29/16.
 */
public class LotResult {
    public static final String ACTIVE = "inprogress";
    public static final String COMPLETED = "deal";
    public static final String REJECTED = "reject";
}
