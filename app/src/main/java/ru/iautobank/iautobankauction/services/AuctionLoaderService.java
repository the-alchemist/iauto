package ru.iautobank.iautobankauction.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telecom.ConnectionService;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.iautobank.iautobankauction.helpers.AuctionHelper;
import ru.iautobank.iautobankauction.interfaces.AutoAPI;
import ru.iautobank.iautobankauction.models.Auction;

public class AuctionLoaderService extends Service {

    private ConnectionBinder connectionBinder = new ConnectionBinder();

    private List<Auction> auctions = new ArrayList<>();
    private AuctionHelper helper = AuctionHelper.getInstance(this);

    public AuctionLoaderService() {
        auctions = new ArrayList<>();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return connectionBinder;
    }

    private class ConnectionBinder extends Binder {
        AuctionLoaderService getService() {
            return AuctionLoaderService.this;
        }
    }
}
