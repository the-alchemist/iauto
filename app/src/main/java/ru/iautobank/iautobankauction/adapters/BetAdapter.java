package ru.iautobank.iautobankauction.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.models.Bet;
import ru.iautobank.iautobankauction.models.Lot;

/**
 * Created by thealchemist on 5/29/16.
 */
public class BetAdapter extends RecyclerView.Adapter<BetAdapter.BetViewHolder> {

    List<Bet> bets;

    public List<Bet> getBets() {
        return bets;
    }

    public BetAdapter(Lot currentLot) {
        super();
        this.bets = new ArrayList<>();
    }

    public void loadBets(Lot currentLot) {
        for(Bet b: currentLot.getBets()) {
            this.bets.add(b);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public BetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bet, parent, false);
        return new BetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BetViewHolder holder, int position) {
        holder.betIcon.setImageDrawable(holder.betIcon.getContext().getResources().getDrawable(android.R.drawable.presence_online));
        holder.betSum.setText(String.valueOf(bets.get(position).getSum()));
        holder.betDate.setText(String.valueOf(bets.get(position).getDate()));
    }

    @Override
    public int getItemCount() {
        return bets.size();
    }

    public static class BetViewHolder extends RecyclerView.ViewHolder {

        ImageView betIcon;
        TextView betSum;
        TextView betDate;

        public BetViewHolder(View item) {
            super(item);

            betSum = (TextView) item.findViewById(R.id.bet_sum);
            betDate = (TextView) item.findViewById(R.id.bet_date);
            betIcon = (ImageView) item.findViewById(R.id.bet_icon);

        }
    }
}
