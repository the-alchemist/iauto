package ru.iautobank.iautobankauction.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.models.Lot;
import ru.iautobank.iautobankauction.models.Param;

/**
 * Created by thealchemist on 5/27/16.
 */
public class LotParamAdapter extends RecyclerView.Adapter<LotParamAdapter.LotParamViewHolder> {

    public List<Param> params;
    private Lot lot;


    public LotParamAdapter(Lot lot) {
        super();
        this.lot = lot;
        if(params == null || params.size() == 0) {
            params = new ArrayList<>();
        }
    }

    public void populateParams(Context c) {
        if(lot != null) {
            params.add(new Param(c.getString(R.string.lot_param_mileage), lot.getRun()));
            params.add(new Param(c.getString(R.string.lot_param_body), lot.getBody()));
            params.add(new Param(c.getString(R.string.lot_param_engine_volume), lot.getCapacity()));
            params.add(new Param(c.getString(R.string.lot_param_power), lot.getPower()));
            params.add(new Param(c.getString(R.string.lot_param_transmission), lot.getTransmission()));
            params.add(new Param(c.getString(R.string.lot_param_shop), lot.getShop()));
        }
        this.notifyDataSetChanged();
    }

    @Override
    public LotParamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lot_param, parent, false);
        return new LotParamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LotParamViewHolder holder, int position) {
        holder.textParamKey.setText(String.valueOf(params.get(position).getKey()));
        holder.textParamValue.setText(String.valueOf(params.get(position).getValue()));
    }

    @Override
    public int getItemCount() {
        return params.size();
    }

    public static class LotParamViewHolder extends RecyclerView.ViewHolder {

        TextView textParamKey;
        TextView textParamValue;


        public LotParamViewHolder(View item) {
            super(item);

            textParamKey = (TextView) item.findViewById(R.id.item_param_name);
            textParamValue = (TextView) item.findViewById(R.id.item_param_value);

        }
    }

}
