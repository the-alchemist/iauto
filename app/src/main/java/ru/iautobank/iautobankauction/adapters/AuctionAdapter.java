package ru.iautobank.iautobankauction.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.helpers.AuctionHelper;
import ru.iautobank.iautobankauction.interfaces.AutoAPI;
import ru.iautobank.iautobankauction.listeners.RecyclerViewEventListener;
import ru.iautobank.iautobankauction.models.Auction;

/**
 * Created by thealchemist on 12/17/15.
 */
public class AuctionAdapter extends RecyclerView.Adapter<AuctionAdapter.AuctionViewHolder> {

    public List<Auction> auctions;
    private static RecyclerViewEventListener listener;
    private AuctionHelper helper;

    public AuctionAdapter(Context context, RecyclerViewEventListener listener) {
        super();
        AuctionAdapter.listener = listener;


        if(auctions == null || auctions.size() == 0) {
            auctions = new ArrayList<>();
            helper = AuctionHelper.getInstance(context);

            loadAuctions();
        }
    }

    public void loadAuctions() {
        auctions.addAll(helper.getAllAuctions());
    }

    public AuctionAdapter(List<Auction> auctions) {
        super();
        this.auctions = auctions;
    }

    @Override
    public AuctionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_auction, parent, false);
        return new AuctionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AuctionViewHolder holder, int position) {
        Glide.with(holder.auctionImageView.getContext()).load(auctions.get(position).getImageUrl()).into(holder.auctionImageView);
        holder.auctionNumber.setText(String.valueOf("#" + auctions.get(position).getIdString()));
        holder.auctionPublishedDate.setText(String.valueOf(auctions.get(position).getCreatedAt()));
        holder.auctionName.setText(String.valueOf(auctions.get(position).getTitle()));
        holder.auctionPrice.setText(String.valueOf(auctions.get(position).getLastPriceFormatted()));
    }

    @Override
    public int getItemCount() {
        return auctions.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class AuctionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View view;

        ImageView auctionImageView;
        TextView auctionNumber;
        TextView auctionName;
        TextView auctionPrice;
        TextView auctionPublishedDate;

        public AuctionViewHolder(View item) {
            super(item);

            auctionImageView = (ImageView) item.findViewById(R.id.item_preview);
            auctionImageView.setScaleType(ImageView.ScaleType.FIT_XY);
            auctionNumber = (TextView) item.findViewById(R.id.item_lot_number);
            auctionName = (TextView) item.findViewById(R.id.item_name);
            auctionPrice = (TextView) item.findViewById(R.id.item_price);
            auctionPublishedDate = (TextView) item.findViewById(R.id.item_year_value);
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onRecyclerViewItemClicked(v, this.getLayoutPosition());
        }
    }
}
