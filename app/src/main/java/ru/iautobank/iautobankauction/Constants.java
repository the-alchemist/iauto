package ru.iautobank.iautobankauction;

import android.provider.BaseColumns;

/**
 * Created by thealchemist on 12/17/15.
 */
public class Constants {

    //API constants
    public static final String API_URL = "http://iautobank.ru/api/";
    public static final String API_IMAGE_URL = "http://iautobank.ru/site/img?id=";

    //Database constants
    public static final String DB_NAME = "iauto2.db";
    public static final int DB_VERSION = 1;

    //Database table names
    public static final String AUCTION_TABLE = "auctions";
    public static final String LOT_TABLE = "lots";
    public static final String BET_TABLE = "bets";

    //Database keys
    public static final String KEY_ID = "id";
    public static final String KEY_MODEL = "model";
    public static final String KEY_BRAND = "brand";
    public static final String KEY_YEAR = "year";
    public static final String KEY_TITLE = "title";
    public static final String KEY_LASTPRICE = "lastPrice";
    public static final String KEY_LASTPRICEFORMATTED = "lastPriceFormatted";
    public static final String KEY_LASTDELTA = "lastDelta";
    public static final String KEY_BETCOUNT = "betCount";
    public static final String KEY_FILEID = "fileId";
    public static final String KEY_IMAGEURL = "imageUrl";
    public static final String KEY_DATEUPDATE = "dateUpdate";
    public static final String KEY_CREATEDAT = "createdAt";
    public static final String KEY_DATEEND = "dateEnd";
    public static final String KEY_RESULT = "result";
    public static final String KEY_LASTBET = "lastBet";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_IMAGES = "images";
    public static final String KEY_DATE = "date";
    public static final String KEY_COMMENT = "comment";
    public static final String KEY_RUN = "run";
    public static final String KEY_BODY = "body";
    public static final String KEY_CAPACITY = "capacity";
    public static final String KEY_POWER = "power";
    public static final String KEY_TRANSMISSION = "transmission";
    public static final String KEY_SHOP = "shop";
    public static final String KEY_BETS = "bets";
    public static final String KEY_SUM = "sum";


    /* Database table creating scripts */
    public static final String AUCTION_TABLE_CREATE_SCRIPT = "create table auctions " +
            "(" +
            BaseColumns._ID + " integer primary key autoincrement, "
            + KEY_ID + " integer, "
            + KEY_MODEL + " text not null, "
            + KEY_BRAND + " text not null, "
            + KEY_YEAR + " text not null, "
            + KEY_TITLE + " text not null, "
            + KEY_LASTPRICE + " integer, "
            + KEY_LASTPRICEFORMATTED + " text not null, "
            + KEY_LASTDELTA + " integer, "
            + KEY_BETCOUNT + " integer, "
            + KEY_FILEID + " integer, "
            + KEY_IMAGEURL + " text not null, "
            + KEY_DATEUPDATE + " text not null, "
            + KEY_CREATEDAT + " text not null, "
            + KEY_DATEEND + " text"
            + ");";



    public static final String LOT_TABLE_CREATE_SCRIPT = "create table lots " +
            "(" +
            BaseColumns._ID + " integer primary key autoincrement, "
            + KEY_ID + " integer, "
            + KEY_RESULT + " text not null, "
            + KEY_MODEL + " text not null, "
            + KEY_BRAND + " text not null, "
            + KEY_YEAR  + " text not null, "
            + KEY_LASTBET + " integer, "
            + KEY_IMAGE + " integer, "
            + KEY_IMAGES + " text not null, "
            + KEY_DATE + " text not null, "
            + KEY_COMMENT + " text not null, "
            + KEY_RUN + " text not null, "
            + KEY_BODY + " text not null, "
            + KEY_CAPACITY + " text not null, "
            + KEY_POWER + " text not null, "
            + KEY_TRANSMISSION + " text not null, "
            + KEY_SHOP + " text not null, "
            + KEY_BETS + " text not null"
            + ");";

    public static final String BET_TABLE_CREATE_SCRIPT = "create table bets " +
            "(" +
            BaseColumns._ID + " integer primary key autoincrement, "
            + KEY_ID + " integer, "
            + KEY_DATE + " text not null, "
            + KEY_SUM + " text not null, "
            + ");";

    // Database queries
    public static final String QUERY_GET_AUCTIONS = "SELECT * FROM auctions; ";
    public static final String QUERY_GET_LOTS = "SELECT * FROM lots; ";
    public static final String QUERY_GET_BETS = "SELECT * FROM bets; ";

    public static final String QUERY_DROP_AUCTIONS = "DROP TABLE IF EXISTS auctions; ";
    public static final String QUERY_DROP_LOTS = "DROP TABLE IF EXISTS lots; ";
    public static final String QUERY_DROP_BETS = "DROP TABLE IF EXISTS bets; ";




}
