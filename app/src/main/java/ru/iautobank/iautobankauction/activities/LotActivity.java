package ru.iautobank.iautobankauction.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.iautobank.iautobankauction.Constants;
import ru.iautobank.iautobankauction.DividerItemDecoration;
import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.adapters.BetAdapter;
import ru.iautobank.iautobankauction.adapters.LotParamAdapter;
import ru.iautobank.iautobankauction.interfaces.AutoAPI;
import ru.iautobank.iautobankauction.models.Bet;
import ru.iautobank.iautobankauction.models.Lot;

public class LotActivity extends AppCompatActivity {


    Lot currentLot;

    LotParamAdapter lotParamAdapter;
    BetAdapter betAdapter;

    EditText dialogBetEdit;
    TextView lotPriceTextView, lotToolbarIdText, lotBetsLabel, lotCommentText;
    ImageView lotImageView;

    CollapsingToolbarLayout collapsingToolbarLayout;

    private class FetchLot extends AsyncTask<Void,Void,Void> {

        private int id = 0;

        public FetchLot(int id) {
            this.id = id;
        }

        @Override
        protected Void doInBackground(Void... params) {
            AutoAPI.Factory.getInstance().getLot(id).enqueue(new Callback<Lot>() {
                @Override
                public void onResponse(Call<Lot> call, Response<Lot> response) {

                    currentLot = response.body();
                    setLotView(currentLot);
                }

                @Override
                public void onFailure(Call<Lot> call, Throwable t) {

                }
            });
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lot);

        lotPriceTextView = (TextView) findViewById(R.id.lot_price);
        lotCommentText = (TextView) findViewById(R.id.lot_comment);
        lotToolbarIdText = (TextView) findViewById(R.id.lot_toolbar_id);
        lotImageView = (ImageView) findViewById(R.id.lot_toolbar_image);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.lot_collapsing_toolbar);


        Intent i = getIntent();
        int id = i.getIntExtra("ITEM_ID", -1);



        final FloatingActionButton lotBetsButton = (FloatingActionButton) findViewById(R.id.lot_check_button);
        if (lotBetsButton != null) {
            lotBetsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showBetDialog();
                }
            });
        }

        new FetchLot(id).execute();


    }

    private void setLotView(Lot currentLot) {
        lotParamAdapter = new LotParamAdapter(currentLot);
        RecyclerView recyclerParams = (RecyclerView) findViewById(R.id.list_car_params);
        recyclerParams.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        recyclerParams.setAdapter(lotParamAdapter);

        if (lotToolbarIdText != null && lotPriceTextView != null && lotImageView != null) {
            collapsingToolbarLayout.setTitle(String.valueOf(currentLot.getTitle()));
            lotToolbarIdText.setText(String.valueOf("#" + currentLot.getId()));
            lotPriceTextView.setText(currentLot.getLastBet());

            Glide.with(lotImageView.getContext()).load(Constants.API_IMAGE_URL + currentLot.getImage()).into(lotImageView);

            if(currentLot.getComment() == "") {
                lotCommentText.setText(currentLot.getComment());
            } else {
                lotCommentText.setText("Отсутствует.");
            }

        }

        setFontSize();

        lotParamAdapter.populateParams(getApplicationContext());

        betAdapter = new BetAdapter(currentLot);

        RecyclerView recyclerBets = (RecyclerView) findViewById(R.id.list_car_bets);
        recyclerBets.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        recyclerBets.setAdapter(betAdapter);

        betAdapter.loadBets(currentLot);

        if(betAdapter.getItemCount() == 0 || betAdapter == null) {
            lotBetsLabel = (TextView) findViewById(R.id.lot_bets_label);
            lotBetsLabel.setText(getString(R.string.lot_bet_no_bets));
        }

    }

    private void setFontSize() {

        if (collapsingToolbarLayout.getTitle().toString().length() > 10) {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        } else if (collapsingToolbarLayout.getTitle().toString().length() > 20) {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.MinifiedAppBar);
        }
    }

    private void showBetDialog() {

        LayoutInflater inflater = LayoutInflater.from(this);
        View betDialogLayout = inflater.inflate(R.layout.dialog_bet_make, (ViewGroup) findViewById(R.id.dialog_bet_root_layout));

        final EditText field = (EditText) betDialogLayout.findViewById(R.id.dialog_bet_field);
        field.requestFocus();
        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        final AlertDialog.Builder betDialogBuilder = new AlertDialog.Builder(this);
        betDialogBuilder.setView(betDialogLayout);
        betDialogBuilder.setTitle(getString(R.string.dialog_bet_title));
        betDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeBet(Integer.parseInt(field.getText().toString()));
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.dismiss();
            }
        });
        betDialogBuilder.setNegativeButton(getString(R.string.dialog_bet_button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.cancel();
            }
        });

        final AlertDialog dialog = betDialogBuilder.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);

        field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() < 1) {
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    private void makeBet(int amount) {
        if(amount < 1000) amount = 1000;
        int previousBetId = betAdapter.getBets().get(betAdapter.getBets().size() > 0 ? betAdapter.getBets().size() - 1 : 12345).getId();
        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("сегодня в HH:mm");
        String date = simpleDateFormat.format(currentDate);
        String sum = Integer.parseInt(currentLot.getLastBet().replace(" ", "").substring(0, currentLot.getLastBet().length() - 6)) + amount + " руб.";
        Bet newBet = new Bet().withId(previousBetId).withDate(date).withSum(sum);
        betAdapter.getBets().add(newBet);
        betAdapter.notifyDataSetChanged();
        Toast.makeText(this, "Making bet of " + amount + "...", Toast.LENGTH_SHORT).show();
    }
}
