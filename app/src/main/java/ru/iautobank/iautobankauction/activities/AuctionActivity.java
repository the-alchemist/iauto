package ru.iautobank.iautobankauction.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.iautobank.iautobankauction.DividerItemDecoration;
import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.SettingsActivity;
import ru.iautobank.iautobankauction.adapters.AuctionAdapter;
import ru.iautobank.iautobankauction.helpers.AuctionHelper;
import ru.iautobank.iautobankauction.helpers.LotHelper;
import ru.iautobank.iautobankauction.interfaces.AutoAPI;
import ru.iautobank.iautobankauction.listeners.RecyclerViewEventListener;
import ru.iautobank.iautobankauction.models.Auction;
import ru.iautobank.iautobankauction.models.Lot;

public class AuctionActivity extends AppCompatActivity {

    AuctionAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction);

        adapter = new AuctionAdapter(this, new RecyclerViewEventListener() {
            @Override
            public void onRecyclerViewItemClicked(View v, int position) {
                TextView itemLotNumberTextView = (TextView) v.findViewById(R.id.item_lot_number);

                int itemLotNumber = Integer.parseInt(itemLotNumberTextView.getText().toString().replaceAll("#",""));

                Intent i = new Intent(AuctionActivity.this, LotActivity.class);
                i.putExtra("ITEM_ID", itemLotNumber);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                startActivity(i);
            }

            @Override
            public void onRecyclerViewItemDismissed(View v, int position) {

            }
        });

        RecyclerView recyclerAuctionList = (RecyclerView) findViewById(R.id.list_auctions);
        if (recyclerAuctionList != null) {
            recyclerAuctionList.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
            recyclerAuctionList.setAdapter(adapter);
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                AutoAPI.Factory.getInstance().getAuctions().enqueue(new Callback<List<Auction>>() {
                    @Override
                    public void onResponse(Call<List<Auction>> call, Response<List<Auction>> response) {
                        //TODO: all connections in service!!!
                        List<Auction> auctions = new ArrayList<>();

                        //load lots
                        for (final Auction a : auctions) {
                            AutoAPI.Factory.getInstance().getLot(a.getId()).enqueue(new Callback<Lot>() {
                                @Override
                                public void onResponse(Call<Lot> call, Response<Lot> response) {
                                    Lot lot = response.body();
                                    LotHelper helper = LotHelper.getInstance(getApplicationContext());
                                    helper.addLot(lot);
                                }

                                @Override
                                public void onFailure(Call<Lot> call, Throwable t) {

                                }
                            });
                        }

                        AuctionHelper helper = AuctionHelper.getInstance(getApplicationContext());
                        auctions.addAll(response.body());
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<List<Auction>> call, Throwable t) {

                    }
                });
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(AuctionActivity.this, SettingsActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}