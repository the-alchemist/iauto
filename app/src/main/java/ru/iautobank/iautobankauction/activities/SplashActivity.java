package ru.iautobank.iautobankauction.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.iautobank.iautobankauction.NetworkUtils;
import ru.iautobank.iautobankauction.R;
import ru.iautobank.iautobankauction.helpers.AuctionHelper;
import ru.iautobank.iautobankauction.helpers.LotHelper;
import ru.iautobank.iautobankauction.interfaces.AutoAPI;
import ru.iautobank.iautobankauction.models.Auction;
import ru.iautobank.iautobankauction.models.Lot;

/**
 * Created by thealchemist on 5/17/16.
 */
public class SplashActivity extends AppCompatActivity implements Runnable {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(this, 2000);
    }

    @Override
    public void run() {
        NetworkUtils networkUtils = new NetworkUtils(this);
        AuctionHelper helper = AuctionHelper.getInstance(this);
        FetchData fetchData = new FetchData();

        if(networkUtils.isConnectedToInternet()) {
            fetchData.execute();
        }



        Intent intent = new Intent(SplashActivity.this, AuctionActivity.class);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        startActivity(intent);
        finish();
    }

    /**
     * Created by thealchemist on 6/21/16.
     */
    public class FetchData extends AsyncTask<Void, Integer, Void> {

        private Activity activity;

        public FetchData() {
            this.activity = SplashActivity.this;
        }

        @Override
        protected Void doInBackground(Void... params) {

            //business logic
            //load auctions
            AutoAPI.Factory.getInstance().getAuctions().enqueue(new Callback<List<Auction>>() {
                @Override
                public void onResponse(Call<List<Auction>> call, Response<List<Auction>> response) {
                    //TODO: all connections in service!!!
                    List<Auction> auctions = new ArrayList<>();

                    //load lots
                    for (final Auction a : auctions) {
                        AutoAPI.Factory.getInstance().getLot(a.getId()).enqueue(new Callback<Lot>() {
                            @Override
                            public void onResponse(Call<Lot> call, Response<Lot> response) {
                                Lot lot = response.body();
                                LotHelper helper = LotHelper.getInstance(activity.getApplicationContext());
                                helper.addLot(lot);
                            }

                            @Override
                            public void onFailure(Call<Lot> call, Throwable t) {

                            }
                        });
                    }

                    AuctionHelper helper = AuctionHelper.getInstance(activity.getApplicationContext());
                    auctions.addAll(response.body());
                    helper.addAuctions(auctions);
                    Log.i("!!!", "DATA WAS LOADED");
                }

                @Override
                public void onFailure(Call<List<Auction>> call, Throwable t) {
                    Log.e("API Failure", t.getMessage());
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            TextView textView = (TextView) findViewById(R.id.splash_load_label);
            textView.setText("Загрузка... " + values[0] + "%");
        }
    }
}
