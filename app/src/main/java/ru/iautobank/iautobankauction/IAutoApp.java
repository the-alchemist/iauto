package ru.iautobank.iautobankauction;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import ru.iautobank.iautobankauction.activities.SplashActivity;

/**
 * Created by thealchemist on 5/29/16.
 */

public class IAutoApp extends Application {
    private static IAutoApp instance;

    public IAutoApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        Context context = getInstance().getApplicationContext();
        Intent splashActivity = new Intent(context, SplashActivity.class);
        splashActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(splashActivity);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
