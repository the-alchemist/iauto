package ru.iautobank.iautobankauction.listeners;

import android.view.View;

/**
 * Created by thealchemist on 6/6/16.
 */
public interface RecyclerViewEventListener {
    void onRecyclerViewItemClicked(View v, int position);
    void onRecyclerViewItemDismissed(View v, int position);
}
